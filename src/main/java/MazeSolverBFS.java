import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Exercise 2: Solving a Maze with BFS.
 */
public class MazeSolverBFS {
    /**
     * Adjacency list.
     */
    private final ArrayList<ArrayList<Integer>> edges;

    /**
     * Number of lines in the tile grid.
     */
    private int lineCount;
    /**
     * Number of columns in the tile grid.
     */
    private int columnCount;
    /**
     * Index of the tile from which to start searching a path to the end tile.
     */
    private int startIndex;
    /**
     * Target tile to join from the starting tile.
     */
    private int endIndex;

    /**
     * Number of movements performed by the BFS search, in all directions
     * (not necessarily on the final solution path).
     */
    private int solvingSteps;

    /**
     * Initializes the maze graph by reading the file at the specified path.
     *
     * @param inputPath Path of the file to read.
     */
    public MazeSolverBFS(String inputPath) {
        this.edges = new ArrayList<>();
        parseInput(inputPath);
        this.solvingSteps = 0;
    }

    /**
     * Reads the input file, encoded as per the assignment PDF specifications.
     * This builds the graph that will be used for solving.
     *
     * @param inputPath Path of the file to read.
     */
    private void parseInput(String inputPath) {
        try (InputStream fileStream = new FileInputStream(inputPath)) {
            // The specifications use , and : delimiters. Make the scanner use those
            // as delimiters. It makes it super easy to parse the file's content.
            Scanner scanner = new Scanner(fileStream).useDelimiter("[,:]");
            // Read the maze size and the start/end indices.
            lineCount = scanner.nextInt();
            columnCount = scanner.nextInt();
            startIndex = scanner.nextInt();
            endIndex = scanner.nextInt();

            // Initialize the edge representation. At first, there are no edges.
            final int tileCount = lineCount * columnCount;
            for (int i = 0; i < tileCount; i++) {
                edges.add(new ArrayList<>());
            }

            // Read the edges of the graph.
            String openness = scanner.next();
            for (int i = 0; i < openness.length(); i++) {
                if (openness.charAt(i) == '1')
                    markWalkable(i, i + 1);
                else if (openness.charAt(i) == '2')
                    markWalkable(i, i + columnCount);
                else if (openness.charAt(i) == '3') {
                    markWalkable(i, i + 1);
                    markWalkable(i, i + columnCount);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Stores the fact that it is possible to walk from the first tile
     * to the second. Node that, contrarily to the maze generator, the
     * adjacency list is not spatially minimized. This allows increasing
     * performance a little at the cost of more memory usage.
     *
     * @param from Source node/tile.
     * @param to   Target node/tile.
     */
    private void markWalkable(int from, int to) {
        int source = Math.min(from, to);
        int target = Math.max(from, to);
        edges.get(source).add(target);
        edges.get(target).add(source);
    }

    /**
     * @param from Tile A.
     * @param to   Tile B.
     * @return True if it is possible to go from tile A to tile B
     * in one move (accessible neighbour). Note that this does
     * not take into account whether this node has already been
     * visited or not.
     */
    private boolean isWalkable(int from, int to) {
        return edges.get(from).contains(to);
    }

    /**
     * @param path Path between the start and end node.
     * @param from Vertex A.
     * @param to   Vertex B.
     * @return True if the edge between A and B is part of the path to go from the
     * start node to the end node.
     */
    private boolean isPathEdge(ArrayList<Integer> path, int from, int to) {
        return (from == startIndex || to == startIndex || path.contains(from)) &&
                (from == endIndex || to == endIndex || path.contains(to));
    }

    /**
     * Prints the maze in its graph representation along with the path from the
     * start to the end node.
     *
     * @param path The path found by the BFS.
     */
    private void printMazeGraphWithPath(ArrayList<Integer> path) {
        for (int line = 0; line < lineCount - 1; line++) {
            System.out.printf("%03d |    ", line);
            // Print the edges on this line.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (index == startIndex)
                    System.out.print('A');
                else if (index == endIndex)
                    System.out.print('B');
                else if (path.contains(index))
                    System.out.print('#');
                else
                    System.out.print('o');
                if (col < columnCount - 1) {
                    if (isWalkable(index, index + 1)) {
                        if (isPathEdge(path, index, index + 1))
                            System.out.print("##");
                        else
                            System.out.print("--");
                    } else
                        System.out.print("  ");
                }
            }
            System.out.println();
            System.out.print("    |    ");
            // Print the edges that go below.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (isWalkable(index + columnCount, index)) {
                    if (isPathEdge(path, index, index + columnCount))
                        System.out.print("#  ");
                    else
                        System.out.print("|  ");
                } else
                    System.out.print("   ");
            }
            System.out.println();
        }
        System.out.printf("%03d |    ", lineCount - 1);
        for (int col = 0; col < columnCount; col++) {
            int index = col + (lineCount - 1) * columnCount;
            if (index == startIndex)
                System.out.print("A");
            else if (index == endIndex)
                System.out.print("B");
            else if (path.contains(index))
                System.out.print("#");
            else
                System.out.print("o");
            if (col < columnCount - 1) {
                if (isWalkable(index, index + 1)) {
                    if (isPathEdge(path, index, index + 1))
                        System.out.print("##");
                    else
                        System.out.print("--");
                } else
                    System.out.print("  ");
            }
        }
        System.out.println("\n");
    }

    /**
     * Prints the maze in its grid representation along with the path from the
     * start to the end node. Dashes indicate walls.
     *
     * @param path The path found by the BFS.
     */
    private void printMazeGridWithPath(ArrayList<Integer> path) {
        System.out.println("Maze grid. Dashes indicate walls.");

        // Top wall.
        System.out.println("      " + "---".repeat(columnCount) + "--");

        for (int line = 0; line < lineCount - 1; line++) {
            // Left wall (and line number).
            System.out.printf("%03d   |", line);
            // Print the edges on this line.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (index == startIndex)
                    System.out.print('S');
                else if (index == endIndex)
                    System.out.print('F');
                else if (path.contains(index))
                    System.out.print('*');
                else
                    System.out.print(' ');
                if (col < columnCount - 1) {
                    if (isWalkable(index, index + 1))
                        System.out.print("  ");
                    else
                        System.out.print(" |");
                } else {
                    System.out.print("  |");
                }
            }
            System.out.print("\n      |");
            // Print the edges that go below.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (col < columnCount - 1 && isWalkable(index + columnCount, index)) {
                    System.out.print("  |");
                } else
                    System.out.print("---");
            }
            System.out.println("|");
        }
        System.out.printf("%03d   |", lineCount - 1);
        for (int col = 0; col < columnCount; col++) {
            int index = col + (lineCount - 1) * columnCount;
            if (index == startIndex)
                System.out.print('S');
            else if (index == endIndex)
                System.out.print('F');
            else if (path.contains(index))
                System.out.print('*');
            else
                System.out.print(' ');
            if (col < columnCount - 1) {
                if (isWalkable(index, index + 1))
                    System.out.print("  ");
                else
                    System.out.print(" |");
            } else {
                System.out.print("  |");
            }
        }

        // Bottom wall.
        System.out.println("\n      " + "---".repeat(columnCount) + "--");
    }

    /**
     * Computes the path from the start node to the end node using the parent-children
     * relationship hashmap computed by the BFS.
     *
     * @param parents Keeps track of which node is the parent of which other node. The
     *                key is the CHILD, and the value is its parent. Therefore, there
     *                can be duplicate values.
     * @return List of the indices, ordered from the start node to the end node.
     */
    private ArrayList<Integer> computePath(HashMap<Integer, Integer> parents) {
        ArrayList<Integer> path = new ArrayList<>();
        int index = endIndex;
        while (index != startIndex) {
            path.add(index);
            index = parents.get(index);
        }
        path.add(startIndex);
        Collections.reverse(path);
        return path;
    }

    /**
     * BFS in the graph. It uses a queue for storing the nodes to process
     * and a hashmap to keep track of the parents (see the PDF report).
     *
     * @return List of the vertices in the path from the start to the end node.
     */
    public ArrayList<Integer> solve() {
        // We have the choice between storing a queue of paths,
        // and storing the parents using a hashmap. The latter
        // is used here to keep things simple.

        this.solvingSteps = 0; // In case the function is called twice, reset this before solving.
        Queue<Integer> queue = new LinkedList<>();
        HashMap<Integer, Integer> parents = new HashMap<>();
        queue.add(startIndex);

        while (!queue.isEmpty()) {
            solvingSteps++;
            int index = queue.poll();
            // If we found the node we want, compute the path
            // from the parents hashmap.
            if (index == endIndex) {
                return computePath(parents);
            }

            // Add all the children.
            for (int child : edges.get(index)) {
                if (!parents.containsKey(child)) {
                    queue.add(child);
                    parents.put(child, index);
                }
            }
        }

        System.out.println("Could not find a path between " + startIndex + " and " + endIndex);
        return new ArrayList<>();
    }

    /**
     * Prints both the graph and the grid representations of the maze for small values
     * of n and m, as well as the path vertices, path steps, solving steps and solving
     * time.
     *
     * @param path        Path from the start to the end node in the graph.
     * @param elapsedTime Average time required to find the path.
     */
    public void printResults(ArrayList<Integer> path, long elapsedTime) {
        // If the maze is small enough, print it :)
        if (lineCount <= 10 && columnCount <= 10) {
            printMazeGraphWithPath(path);
            printMazeGridWithPath(path);
        }

        // Print the path.
        System.out.print("Path: ");
        for (int i = 0; i < path.size() - 1; i++) {
            System.out.print(path.get(i) + ", ");
        }
        System.out.println(path.get(path.size() - 1));

        // Print the steps / number of moves.
        int steps = path.size() - 1;
        System.out.println("Steps (number of moves to follow the path): " + steps);

        // Print the number of steps taken to solve.
        System.out.println("Solving Steps (number of moves the algorithm performed in the search): "
                + this.solvingSteps);

        // Print the time taken to solve.
        float elapsedMs = elapsedTime / 1_000_000.0f;
        System.out.println("Solving time (ms):" + elapsedMs);
    }

    /**
     * Parses the command line argument and calls the BFS solver.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Invalid command line arguments.");
            System.out.println("Usage: java MazeSolverBFS INPUT_PATH");
            return;
        }

        try {
            String inputPath = args[0];
            MazeSolverBFS solver = new MazeSolverBFS(inputPath);
            long startTime = System.nanoTime();
            ArrayList<Integer> path = solver.solve();
            for (int i = 0; i < 99999; i++) { // Solve 100K times to get a more precise time measurement.
                solver.solve();
            }
            long elapsed = System.nanoTime() - startTime;
            solver.printResults(path, elapsed / 100000);

        } catch (NumberFormatException ex) {
            System.out.println("Error parsing the input arguments: expected integers for n and m as first 2 arguments.");
        }
    }
}
