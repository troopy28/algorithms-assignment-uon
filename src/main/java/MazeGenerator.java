import java.io.*;
import java.util.*;

/**
 * Exercise 1: Generating a Random Maze.
 */
public class MazeGenerator {

    /**
     * Number of lines in the tile grid.
     */
    private final int lineCount;
    /**
     * Number of columns in the tile grid.
     */
    private final int columnCount;
    /**
     * Path to the file where the maze will be saved.
     */
    private final String outputPath;

    private final Stack<Integer> path;
    /**
     * Set of already visited vertices. Average case: O(1) for insertion and contains() check.
     */
    private final HashSet<Integer> visited;
    /**
     * This stores the neighbours of the current tile. It would have been a prettier design to
     * make this simply the returned value of {@link MazeGenerator#computeAccessibleVertices}.
     * However, it means at every iteration we would need to reallocate the list etc. This member
     * is here to avoid that. (Because clearing up an array list does not deallocate its
     * underlying array.)
     */
    private final ArrayList<Integer> accessibleVertices;
    /**
     * Edge information. For each node in the graph, store the index of the accessible graphs.
     * The index here is the tile index. Not to be confused with the grid representation (which
     * isn't even represented explicitly, it is not needed).
     * <p>
     * An additional property is added to reduce space use: when it is possible to go from index
     * A to index B, A -> B is stored in only one direction, with A being smaller than B. This
     * reduces memory usage by a factor 2 just at the cost of checking the indices.
     * <p>
     * An array of array of indices is used: edges[sourceIndex] = {set of accessible indices}.
     * The set of accessible indices is used in a write-only fashion in the core algorithm and
     * is read sequentially when generating the output. As such, this set is represented using
     * a simple array and not a hashset.
     *
     * @see MazeGenerator#markWalkable(int, int)
     */
    private final ArrayList<ArrayList<Integer>> edges;

    /**
     * Simply initializes the maze generator private members: data structures
     * used to generate the maze, and the adjacency list itself.
     *
     * @param lineCount   Number of lines in the graph.
     * @param columnCount Number of columns in the graph.
     * @param outputPath  File where the graph file should be written to.
     */
    public MazeGenerator(int lineCount, int columnCount, String outputPath) {
        this.lineCount = lineCount;
        this.columnCount = columnCount;
        this.outputPath = outputPath;
        this.path = new Stack<>();
        this.visited = new HashSet<>();
        this.accessibleVertices = new ArrayList<>();
        this.edges = new ArrayList<>();
        // Initialize the edge representation. At first, there are no edges.
        final int tileCount = lineCount * columnCount;
        for (int i = 0; i < tileCount; i++) {
            edges.add(new ArrayList<>());
        }
    }

    /**
     * Computes the list of accessible vertices from the specified position in the
     * graph. A vertex is accessible if it is a neighbour in the grid (left, right,
     * top, bottom) AND if it has not already been visited.
     *
     * @param x     X (horizontal) position in the graph/grid.
     * @param y     Y (vertical) position in the graph/grid.
     * @param index Index in the graph/grid.
     */
    private void computeAccessibleVertices(int x, int y, int index) {
        accessibleVertices.clear();
        if (x > 0) {
            int left = index - 1;
            if (!visited.contains(left))
                accessibleVertices.add(left);
        }
        if (x < columnCount - 1) {
            int right = index + 1;
            if (!visited.contains(right))
                accessibleVertices.add(right);
        }
        if (y > 0) {
            int top = index - columnCount;
            if (!visited.contains(top))
                accessibleVertices.add(top);
        }
        if (y < lineCount - 1) {
            int top = index + columnCount;
            if (!visited.contains(top))
                accessibleVertices.add(top);
        }
    }

    /**
     * Stores the fact that it is possible to walk from the first tile
     * to the second.
     * When it is possible to go from index A to index B, A -> B is
     * stored in only one direction, with A being smaller than B. This
     * reduces memory usage by a factor 2 just at the cost of checking
     * the indices.
     *
     * @param from Source node/tile.
     * @param to   Target node/tile.
     */
    private void markWalkable(int from, int to) {
        int source = Math.min(from, to);
        int target = Math.max(from, to);
        edges.get(source).add(target);
    }

    /**
     * @param from Tile A.
     * @param to   Tile B.
     * @return True if it is possible to go from tile A to tile B
     * in one move (accessible neighbour). Note that this does
     * not take into account whether this node has already been
     * visited or not.
     */
    private boolean isWalkable(int from, int to) {
        int source = Math.min(from, to);
        int target = Math.max(from, to);
        return edges.get(source).contains(target);
    }

    /**
     * Random walk / crawler algorithm. The inner workings of it are explained in the PDF
     * report. It is only reminded that:
     * <ul>
     *     <li>this method directly generates a graph spanning across all vertices in the grid</li>
     *     <li>meaning it is possible to find a path between every pair of vertices in the graph</li>
     *     <li>
     *         it also automatically ensures there is no cycles in the graph without having to
     *         explicitly checking for them in a later step.
     *     </li>
     * </ul>
     */
    private void generate() {
        Random rng = new Random();
        final int tileCount = columnCount * lineCount;
        final int startX = rng.nextInt(columnCount);
        final int startY = rng.nextInt(lineCount);
        final int startIndex = startX + columnCount * startY;
        visited.add(startIndex);
        path.add(startIndex);
        int x = startX;
        int y = startY;

        int index = startIndex;
        int lastVisited = index;
        while (visited.size() < tileCount) {
            computeAccessibleVertices(x, y, index);
            int neighboursCount = accessibleVertices.size();
            // If it isn't possible to go further, backtrack and start a new path.
            if (neighboursCount == 0) {
                index = path.pop();
                y = index / columnCount;
                x = index - (y * columnCount);
            }

            // If it is, choose where to go next randomly.
            else {
                int next = accessibleVertices.get(rng.nextInt(neighboursCount));
                // There is now a path from "index" (current position) to "next".
                markWalkable(index, next);
                // Move to the next tile.
                index = next;
                y = index / columnCount;
                x = index - (y * columnCount);

                visited.add(next);
                path.add(next);
                // Only do this at the end of this branch, not in the backtracking branch.
                lastVisited = index;
            }
        }
        if (columnCount <= 10 && lineCount <= 10) {
            printMazeGraph(startIndex, lastVisited);
            System.out.println();
            System.out.println();
            printMazeGrid(startIndex, lastVisited);
        }
        exportMazeToFile(startIndex, lastVisited);
    }

    /**
     * Exports the maze to the file specified in the constructor. The file
     * is written according to the specifications in the assignment PDF.
     *
     * @param start Start node of the path to search in the BFS solver.
     * @param end   End node of the path to search in the BFS solver.
     */
    void exportMazeToFile(int start, int end) {
        try (FileWriter out = new FileWriter(outputPath)) {
            out.write(String.valueOf(lineCount));
            out.write(",");
            out.write(String.valueOf(columnCount));
            out.write(':');
            out.write(String.valueOf(start));
            out.write(':');
            out.write(String.valueOf(end));
            out.write(':');

            for (int line = 0; line < lineCount; line++) {
                for (int col = 0; col < columnCount; col++) {
                    int index = col + line * columnCount;
                    boolean rightWalkable = col < columnCount - 1 && isWalkable(index, index + 1);
                    boolean bottomWalkable = line < lineCount - 1 && isWalkable(index, index + columnCount);
                    if (rightWalkable) {
                        if (bottomWalkable) {
                            // Both open.
                            out.write('3');
                        } else {
                            // Right only.
                            out.write('1');
                        }
                    } else if (bottomWalkable) {
                        // Bottom only.
                        out.write('2');
                    } else {
                        // Both closed.
                        out.write('0');
                    }
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Could not find the file " + outputPath);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("An error occurred when writing the maze to the specified file " + outputPath);
        }
    }

    /**
     * Prints the maze in its graph representation along with the path from the
     * start to the end node.
     */
    private void printMazeGraph(int startIndex, int endIndex) {
        System.out.println("Maze graph. 'o's are vertices. Dashes indicate it is");
        System.out.println("possible to walk from one vertex to another (NO wall).");

        for (int line = 0; line < lineCount - 1; line++) {
            System.out.printf("%03d |    ", line);
            // Print the edges on this line.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (index == startIndex)
                    System.out.print('S');
                else if (index == endIndex)
                    System.out.print('F');
                else
                    System.out.print('o');
                if (col < columnCount - 1) {
                    if (isWalkable(index, index + 1))
                        System.out.print("--");
                    else
                        System.out.print("  ");
                }
            }
            System.out.println();
            System.out.print("    |    ");
            // Print the edges that go below.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (isWalkable(index + columnCount, index))
                    System.out.print("|  ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
        System.out.printf("%03d |    ", lineCount - 1);
        for (int col = 0; col < columnCount; col++) {
            int index = col + (lineCount - 1) * columnCount;
            if (index == startIndex)
                System.out.print("A");
            else if (index == endIndex)
                System.out.print("B");
            else
                System.out.print("o");
            if (col < columnCount - 1) {
                if (isWalkable(index, index + 1))
                    System.out.print("--");
                else
                    System.out.print("  ");
            }
        }
    }

    /**
     * Prints the maze in its grid representation along with the path from the
     * start to the end node. Dashes indicate walls.
     */
    private void printMazeGrid(int startIndex, int endIndex) {
        System.out.println("Maze grid. Dashes indicate walls.");

        // Top wall.
        System.out.println("      " + "---".repeat(columnCount) + "--");

        for (int line = 0; line < lineCount - 1; line++) {
            // Left wall (and line number).
            System.out.printf("%03d   |", line);
            // Print the edges on this line.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (index == startIndex)
                    System.out.print('S');
                else if (index == endIndex)
                    System.out.print('F');
                else
                    System.out.print(' ');
                if (col < columnCount - 1) {
                    if (isWalkable(index, index + 1))
                        System.out.print("  ");
                    else
                        System.out.print(" |");
                } else {
                    System.out.print("  |");
                }
            }
            System.out.print("\n      |");
            // Print the edges that go below.
            for (int col = 0; col < columnCount; col++) {
                int index = col + line * columnCount;
                if (col < columnCount - 1 && isWalkable(index + columnCount, index)) {
                    System.out.print("  |");
                } else
                    System.out.print("---");
            }
            System.out.println("|");
        }
        System.out.printf("%03d   |", lineCount - 1);
        for (int col = 0; col < columnCount; col++) {
            int index = col + (lineCount - 1) * columnCount;
            if (index == startIndex)
                System.out.print('S');
            else if (index == endIndex)
                System.out.print('F');
            else
                System.out.print(' ');
            if (col < columnCount - 1) {
                if (isWalkable(index, index + 1))
                    System.out.print("  ");
                else
                    System.out.print(" |");
            } else {
                System.out.print("  |");
            }
        }

        // Bottom wall.
        System.out.println("\n      " + "---".repeat(columnCount) + "--");
    }

    /**
     * Parses the command line argument and calls the maze generator.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Invalid command line arguments.");
            System.out.println("Usage: java MazeGenerator N_SIZE M_SIZE OUTPUT_PATH");
            return;
        }

        try {
            int lineCount = Integer.parseInt(args[0]);
            int columnCount = Integer.parseInt(args[1]);
            String outputPath = args[2];
            MazeGenerator generator = new MazeGenerator(lineCount, columnCount, outputPath);
            generator.generate();
        } catch (NumberFormatException ex) {
            System.out.println("Error parsing the input arguments: expected integers for n and m as first 2 arguments.");
        }

    }

}
